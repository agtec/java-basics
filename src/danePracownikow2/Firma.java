package danePracownikow2;

public class Firma {

    public static void main(String[] args) {

        int aktualnaLiczbaPracownikow = 3;

        Pracownik[] pracownicy = new Pracownik[aktualnaLiczbaPracownikow];

        Pracownik eugeniusz = new Pracownik("Eugeniusz", "Kowalski");
        Pracownik zofia = new Pracownik("Zofia", "Nowak", 29);
        Pracownik jan = new Pracownik("Sobieski");

        pracownicy[0] = eugeniusz;
        pracownicy[1] = zofia;
        pracownicy[2] = jan;

        for (Pracownik pracownik:pracownicy) {
            System.out.println();
            System.out.print("imie: " + pracownik.getImie());
            System.out.print(", nazwisko: " + pracownik.getNazwisko());
            System.out.print(", wiek: " + pracownik.getWiek());

        }


    }
}
