package liczydelko;

public class LiczydelkoTest {

    public static void main(String[] args) {

        Liczydelko suma1 = new Liczydelko();

        System.out.println("int dodaj (int a, int b, int c): " + suma1.dodaj(9, 5, 6));
        System.out.println("double dodaj (int a, double b): " + suma1.dodaj(2, 33.5));
        System.out.println("float dodaj (float a, int b, int c): " + suma1.dodaj(1235432F, 5, 1));
        System.out.println("double dodaj (double a, double b): " + suma1.dodaj(3.0, 5.7));

    }
}
