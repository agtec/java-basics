package liczydelko;

public class Liczydelko {

    public int dodaj (int a, int b, int c) {
        int wynik = a + b + c;
        return wynik;
    }

    public double dodaj (int a, double b) {
        double wynik = a + b;
        return wynik;
    }

    public float dodaj (float a, int b, int c) {
        float wynik = a + b + c;
        return  wynik;
    }

    public double dodaj (double a, double b) {
        double wynik = a + b;
        return wynik;
    }
}
