package danePracownikow;

public class Pracownik {

    private int pesel;
    private char plec;
    private int wiek;

    Pracownik() {

    }

    Pracownik(int pesel, char plec, int wiek) {
        this.pesel = pesel;
        this.plec = plec;
        this.wiek = wiek;
    }

    public int getPesel() {
        return pesel;
    }

    public void setPesel(int pesel) {
        this.pesel = pesel;
    }

    public char getPlec() {
        return plec;
    }

    public void setPlec(char plec) {
        this.plec = plec;
    }

    public int getWiek() {
        return wiek;
    }

    public void setWiek(int wiek) {
        this.wiek = wiek;
    }
}

