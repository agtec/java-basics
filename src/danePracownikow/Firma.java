package danePracownikow;

public class Firma {


    public Pracownik utworzPracownika(int peselPracownika, char plecPracownika, int wiekPracownika) {
        Pracownik pracownik = new Pracownik(peselPracownika, plecPracownika, wiekPracownika);

        return pracownik;

    }

    public void pokazDanePracownika(Pracownik pracownik) {
        System.out.println("Dane pracownika: ");
        System.out.print("pesel: " + pracownik.getPesel());
        if (pracownik.getPlec() == 'k') {
            System.out.print(", plec: kobieta");
        } else if (pracownik.getPlec() == 'm') {
            System.out.print(", plec: mezczyzna");
        } else {
            System.out.print("plec inna?");
        }
        System.out.print(", wiek: " + pracownik.getWiek());
        System.out.println();
    }


    public static void main(String[] args) {

        Firma company = new Firma();

        Pracownik pracownik1 = company.utworzPracownika(1234567, 'k', 27);
        Pracownik pracownik2 = company.utworzPracownika(83764, 'm', 89);

        company.pokazDanePracownika(pracownik1);
        company.pokazDanePracownika(pracownik2);

    }

}
