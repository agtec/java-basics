package rownanieKwadratowe;

public class Wynik {

    private double x1;
    private double x2;

    public double getX1() {
        return x1;
    }

    public void setX1(double x1) {
        this.x1 = x1;
    }

    public double getX2() {
        return x2;
    }

    public void setX2(double x2) {
        this.x2 = x2;
    }

    public Wynik(double x1, double x2) {
        this.x1 = x1;
        this.x2 = x2;
    }

    @Override
    public String toString() {
        if (x1 == x2) {
            return "Rozwiazaniem równania jest: x: " + x1;
        }
        return "Rozwiazaniem równania jest: x1: " + x1 + " oraz x2: " + x2;
    }
}
