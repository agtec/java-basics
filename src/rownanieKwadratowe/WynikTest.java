package rownanieKwadratowe;

import java.util.Scanner;

public class WynikTest {

    // program do rozwiazywania r-nia kwadratowego, y = ax2 + bx + c

    public double obliczDelte(double a, double b, double c) {
        double delta = (b * b) - 4 * (a * c);
        return delta;
    }

    public Wynik obliczX() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj a: ");
        double a = sc.nextDouble();
        System.out.println("Podaj b: ");
        double b = sc.nextDouble();
        System.out.println("Podaj c: ");
        double c = sc.nextDouble();

        double wynik = obliczDelte(a, b, c);

        double x1;
        double x2;

        if (wynik > 0) {
            x1 = ((-b - (Math.sqrt(wynik))) / 2 * a);
            x2 = ((-b + (Math.sqrt(wynik))) / 2 * a);
            return new Wynik(x1, x2);
        } else if (wynik == 0) {
            x1 = (-b / 2 * a);
            x2 = (-b / 2 * a);
            return new Wynik(x1, x2);
        }
        return null;
    }

    public static void main(String[] args) {

        WynikTest obj = new WynikTest();
        Wynik wynik = obj.obliczX();

        if (wynik != null) {
            System.out.println(wynik);
        } else {
            System.out.println("Nie ma rozwiazania");
        }
    }
}
