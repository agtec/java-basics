package wzory;

import java.util.Scanner;

public class WzorGwiazdek {

    public void rysujGwiazki() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj ilosc gwiazdek: ");
        int ilosc = sc.nextInt();

        String spacje = "";
        int iloscSpacji = ilosc;
        int indeksSpacji = ilosc - 1;
        String gwiazdka = "*";
        String gwiazdki = "**";

        for (int i = 0; i < ilosc; i++) {
            spacje += " ";
        }

        for (int i = 0; i < ilosc; i++) {
            System.out.println(spacje + gwiazdka + spacje);
            gwiazdka += gwiazdki;
            spacje = spacje.substring(0, indeksSpacji);
            indeksSpacji--;
            if (gwiazdka.length() > ilosc) {
                break;
            }
        }

        // System.out.println("dl spacji" + spacje.length());

        spacje += " ";
        int indeksGwiazdek = ilosc - 2;

        while (gwiazdki.length() > 1) {
            spacje += " ";
            gwiazdka = gwiazdka.substring(0, indeksGwiazdek);
            System.out.println(spacje + gwiazdka + spacje);
            indeksGwiazdek -= 2;
            if (gwiazdka.length() == 1) {
                break;
            }
        }
    }

    public static void main(String[] args) {

        WzorGwiazdek rysunek = new WzorGwiazdek();
        rysunek.rysujGwiazki();
    }
}
