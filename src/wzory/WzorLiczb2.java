package wzory;

import java.util.Scanner;

public class WzorLiczb2 {

    public void wzor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int a = sc.nextInt();
        int b = 1;
        int c = 1;

        for (int i = 1; i < a + 1; i++) {
            System.out.print(i+ " ");
            b--;
            if (b == 0) {
                System.out.println();
                c++;
                b = c;
            }
        }
    }

    public static void main(String[] args) {

        WzorLiczb2 y = new WzorLiczb2();
        y.wzor();
    }
}
