package wzory;

import java.util.Scanner;

public class WzorLiczb3 {

    public void wzor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int x = sc.nextInt();

        String spacje = "";

        for (int s = 0; s < x; s++) {
            spacje += " ";
        }

        int y = x - 1;
        String str;

        for (int i = 1; i < x + 1; i++) {
            str = String.valueOf(i);
            System.out.print(spacje);
            for (int j = 1; j <= i; j++) {
                System.out.print(i + " ");
            }
            System.out.print(spacje);
            if (y < 0) {
                spacje = "";
            } else {
                spacje = spacje.substring(0, y);
            }
            y--;

            System.out.println();
        }
    }

    public static void main(String[] args) {

        WzorLiczb3 z = new WzorLiczb3();
        z.wzor();
    }
}
