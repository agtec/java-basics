package wzory;

import java.util.Scanner;

public class WzorLiczb1 {

    public void wzor() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int a = sc.nextInt();

        for(int i=0; i<a; i++) {
            for (int j=0; j<i+1; j++) {
                System.out.print((i+1));
            }
            System.out.println();
        }
    }

    public static void main(String[] args) {
        WzorLiczb1 x = new WzorLiczb1();
        x.wzor();
    }


}
