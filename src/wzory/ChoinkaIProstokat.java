package wzory;

public class ChoinkaIProstokat {

    int ilosc_gwiazdek;

    ChoinkaIProstokat(int ilosc_gwiazdek) {
        this.ilosc_gwiazdek = ilosc_gwiazdek;
    }

    public void rysuj() {

        //choinka

        int ilosc_spacji = (this.ilosc_gwiazdek / 2);
        String spacje = "";

        for (int i = 0; i < ilosc_spacji; i++) {
            spacje += " ";
        }
        String gwiazdki = "*";

        System.out.println("Choinka");

        while (gwiazdki.length() <= this.ilosc_gwiazdek) {
            System.out.println(spacje + gwiazdki + spacje);
            gwiazdki += "**";
            if (spacje.length() > 0) {
                spacje = spacje.substring(1);
            }
        }

        System.out.println("Prostokat");

        // prostokat

        spacje = "";
        gwiazdki = "";

        ilosc_spacji = this.ilosc_gwiazdek - 2;
        for (int i = 0; i < ilosc_spacji; i++) {
            spacje += " ";
        }

        for (int i = 0; i < this.ilosc_gwiazdek; i++) {
            gwiazdki += "*";
        }

        System.out.println(gwiazdki);

        for (int i = 0; i < this.ilosc_gwiazdek - 2; i++) {
            System.out.println("*" + spacje + "*");
        }

        if (this.ilosc_gwiazdek != 1) {
            System.out.println(gwiazdki);
        }
    }
}
