package najwiekszaWartosc;

import java.util.Scanner;

public class NajwiekszaWartosc {

    int a, b, c;
    int max;

    public void pobierzOdUzytkownika () {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj trzy liczby.");
        System.out.println("Wpisz pierwsza liczbe: ");
        a = sc.nextInt();
        System.out.println("Wpisz druga liczbe: ");
        b = sc.nextInt();
        System.out.println("Wpisz trzecia liczbe: ");
        c = sc.nextInt();
    }

    public int KtoraWieksza() {
        pobierzOdUzytkownika();
        max = a;
        if (b>max && b>c) {
            max = b;
        } else if (c>max && c>b) {
            max = c;
        }else {
            max = a;
        }
        return max;
    }


    public static void main(String[] args) {

        NajwiekszaWartosc obj = new NajwiekszaWartosc();

        System.out.println("Najwieksza wartość to: " + obj.KtoraWieksza());
    }
}
