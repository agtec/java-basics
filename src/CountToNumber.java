import java.util.Scanner;

public class CountToNumber {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the number: ");
        float number = sc.nextInt();
        float y = 0;

        System.out.println("Using while: ");

        if (number > 0) {
            while (y <= number + 0.1) {
                System.out.printf("%.1f ", y);
                y += 0.1;
            }
        } else if (number < 0) {
            while (y > number - 0.1) {
                System.out.printf("%.1f ", y);
                y -= 0.1;
            }
        } else if (number == 0) {
            System.out.println(number);
        }

        System.out.println();
        System.out.println("Using for: ");


        if (number > 0) {
            for (float i = 0; i <= number + 0.1; i += 0.1) {
                System.out.printf("%.1f ", i);
            }
        } else if (number < 0) {
            for (float i = 0; i > number - 0.1; i -= 0.1) {
                System.out.printf("%.1f ", i);
            }
        } else if (number == 0) {
            System.out.println(number);
        }
    }
}
