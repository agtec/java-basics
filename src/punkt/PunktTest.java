package punkt;

public class PunktTest {

    public static void main(String[] args) {

        Punkt punkt1 = new Punkt(1, 3);

        System.out.println("Metoda zwroc x i y");

        System.out.println(punkt1.zwrocX());
        System.out.println(punkt1.zwrocY());

        System.out.println("Metoda zwieksz x i y o 1");

        System.out.println(punkt1.zwiekszXo1());
        System.out.println(punkt1.zwiekszYo1());

        System.out.println("Metoda wyswietl x i y");

        punkt1.wyswietlX();
        punkt1.wyswietlY();
    }
}
