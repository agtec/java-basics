package punkt;

public class Punkt {

    private int x;
    private int y;

    Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int zwiekszXo1() {
        return ++x;
    }

    public int zwiekszYo1() {

        return ++y;
    }

    public void zwiekszODowolnaWartosc(int zmienna, int i) {
        if (x == zmienna) {
            x = x + i;
        } else if (y == zmienna) {
            y = y + i;
        }

    }


    public int zwrocX() {
        return x;
    }


    public int zwrocY() {
        return y;
    }


    public void wyswietlX() {
        System.out.print("Wartosc x wynosi: ");
        System.out.println(zwrocX());
    }


    public void wyswietlY() {
        System.out.print("Wartosc y wynosi: ");
        System.out.println(zwrocY());
    }

}
