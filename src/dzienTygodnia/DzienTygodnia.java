package dzienTygodnia;

public enum DzienTygodnia {

    PONIEDZIALEK (1),
    WTOREK (2),
    SRODA (3),
    CZWARTEK(4),
    PIATEK(5),
    SOBOTA(6),
    NIEDZIELA(7);

    int pozycja;

    DzienTygodnia(int pozycja) {
        this.pozycja = pozycja;
    }

    public int getPozycja() {
        return pozycja;
    }

    public void setPozycja(int pozycja) {
        this.pozycja = pozycja;
    }

    public DzienTygodnia ktoryDzien (int pozycja) {
        for (DzienTygodnia dzienTyg : DzienTygodnia.values()) {
            if(dzienTyg.getPozycja() == pozycja) {
                return dzienTyg;
            }
        }
        return null;
    }
}
