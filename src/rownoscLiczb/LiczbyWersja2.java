package rownoscLiczb;

import java.util.Scanner;

public class LiczbyWersja2 {

    public void CzyRowne () {
        Scanner sc = new Scanner(System.in);
        System.out.println("Wpisz pierwsza liczbe: ");
        float a = sc.nextFloat();
        System.out.println("Wpisz druga liczbe: ");
        float b = sc.nextFloat();

        a = a*1000;
        b = b*1000;

        int x = (int)a;
        int y = (int)b;

        if (x==y) {
            System.out.println("liczby sa rowne");
        } else {
            System.out.println("liczby nie sa rowne");
        }
    }

    public static void main(String[] args) {

        LiczbyWersja2 liczby = new LiczbyWersja2();
        liczby.CzyRowne();
    }
}
