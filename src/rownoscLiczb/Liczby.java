package rownoscLiczb;

import java.util.Arrays;

public class Liczby {

    public boolean czyRowne(float liczba1, float liczba2) {

        String l1 = String.valueOf(liczba1);
        String l2 = String.valueOf(liczba2);

        String[] l1_tab = l1.split("\\.");
        String[] l2_tab = l2.split("\\.");

//        System.out.println(Arrays.toString(l1_tab));
//        System.out.println(Arrays.toString(l2_tab));

        boolean czyPrzedPrzecinkiemRowne = false;
        boolean czyPoPrzecinkuRowne = false;
        int cyfryPoPrzecinku1;
        int cyfryPoPrzecinku2;

        if (l1_tab[0].equals(l2_tab[0])) {
            czyPrzedPrzecinkiemRowne = true;
            if (l1_tab[1].equals(l2_tab[1])) {
                czyPoPrzecinkuRowne = true;
            } else {
                if (l1_tab[1].length() > 3) {
                    String liczbaPoPrzecinku1 = l1_tab[1].substring(0, 2);
                    cyfryPoPrzecinku1 = Integer.parseInt(liczbaPoPrzecinku1);
                } else {
                    cyfryPoPrzecinku1 = Integer.parseInt(l1_tab[1]);
                }
                if (l2_tab[1].length() > 3) {
                    String liczbaPoPrzecinku2 = l2_tab[1].substring(0, 2);
                    cyfryPoPrzecinku2 = Integer.parseInt(liczbaPoPrzecinku2);
                } else {
                    cyfryPoPrzecinku2 = Integer.parseInt(l2_tab[1]);
                }
//                System.out.println("porownuje liczby po przecinku " + cyfryPoPrzecinku1 + " oraz " + cyfryPoPrzecinku2);
                if (cyfryPoPrzecinku1 == cyfryPoPrzecinku2) {
                    czyPoPrzecinkuRowne = true;
                }
            }
        }

        if (czyPrzedPrzecinkiemRowne && czyPoPrzecinkuRowne) {
            return true;
        } else {
            return false;
        }
    }

    public static void main(String[] args) {

        Liczby liczby = new Liczby();
        System.out.println(liczby.czyRowne(5, 5));
        System.out.println(liczby.czyRowne(124.5509F, 100.9990F));
        System.out.println(liczby.czyRowne(124.5509F, 124.5501F));
        System.out.println(liczby.czyRowne(124.5509F, 124.55F));
        System.out.println(liczby.czyRowne(124.5509F, 124.550F));
        System.out.println(liczby.czyRowne(5F, 5.000F));
    }
}
