package operacjeNaLiczbach;

import java.util.Scanner;

public class KwadratLiczb {

    public void obliczKwadratLiczb(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj liczbe: ");
        int a = sc.nextInt();

        for(int i=0; i<a; i++) {
            System.out.println((i+1)*(i+1));
        }
    }

    public static void main(String[] args) {
        KwadratLiczb liczba = new KwadratLiczb();
        liczba.obliczKwadratLiczb();
    }
}
