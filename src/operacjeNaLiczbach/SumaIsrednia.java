package operacjeNaLiczbach;

import java.util.Scanner;

public class SumaIsrednia {

    public void obliczSumeiSrednia() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj kolejno pięc liczb: ");
        int suma = 0;
        for (int i = 0; i < 5; i++) {
            System.out.println("Liczba nr " + (i + 1) + ": ");
            int a = sc.nextInt();
            suma += a;
        }
        double srednia = suma / 5;
        System.out.println("Suma podanych liczb: " + suma + ", srednia: " + srednia);
    }

    public static void main(String[] args) {

        SumaIsrednia liczby = new SumaIsrednia();
        liczby.obliczSumeiSrednia();
    }
}
