package dystrybutor;

import java.util.Random;
import java.util.Scanner;

public class Dystrybutor {

    private double ilosc = 0;
    private double cena = 5.0;

    public Dystrybutor() {
    }

    public double getIlosc() {
        return ilosc;
    }

    public void setIlosc(double ilosc) {
        this.ilosc = ilosc;
    }

    Scanner sc = new Scanner(System.in);

    public void tankuj(Auto auto) {
        System.out.println("Rozpoczeto tankowanie...");
        napelniamBak();
    }

    private double napelniamBak() {
        Random r = new Random();
        String decyzja;
        double dodatkowaIloscPaliwa;
        for (int i = 0; i < 151; i++) {
            ilosc = i;
            System.out.println(ilosc);
            if (i == 50 || i == 100) {
                System.out.println("Czy tankowac dalej? t/n");
                decyzja = sc.next();
                if (decyzja.charAt(0) == 't' && ilosc < 150) {
                    System.out.println(ilosc);
                    dodatkowaIloscPaliwa = r.nextInt(51);
                    ilosc += dodatkowaIloscPaliwa;
                    continue;
                } else if (decyzja.charAt(0) == 'n') {
                    dodatkowaIloscPaliwa = r.nextInt(51);
                    ilosc += dodatkowaIloscPaliwa;
                    System.out.println("Zakonczono tankowanie");
                    wyswietlInformacje();
                    break;
                } else {
                    System.out.println("Wpisano nieprawidlowy komunikat");
                    //decyzja = sc.next();
                    wyswietlInformacje();
                    break;
                }
            }

        }
        wyswietlInformacje();
        return ilosc;
    }

    public void wyswietlInformacje() {
        System.out.println("Zatankowano: " + ilosc + " jednostek paliwa");
        double rachunek = ilosc * this.cena;
        System.out.println("Nalezy zaplacic " + rachunek + " zl");
    }
}
