package tablice;

import java.util.Arrays;
import java.util.Scanner;

public class TablicaBezDuplikatow {

    public boolean czyZawiera(int tab[], int wartosc) {
        for (int i = 0; i < tab.length; i++) {
            if (tab[i] == wartosc) {
                return true;
            }
        }
        return false;
    }

    public void usunDuplikaty() {
        Scanner sc = new Scanner(System.in);
        System.out.println("podaj rozmiar tablicy: ");
        int rozmiar = sc.nextInt();
        int[] tab = new int[rozmiar];
        System.out.println("Podaj kolejno wartosci: ");
        for (int i = 0; i < rozmiar; i++) {
            System.out.println("Wpisz wartosc nr: " + (i + 1));
            int x = sc.nextInt();
            tab[i] = x;
        }

        int[] tabBezPotworzen = new int[rozmiar];
        int indeksBezPowtorzen = 0;

        for (int i = 0; i < rozmiar; i++) {
            if (czyZawiera(tabBezPotworzen, tab[i]
            )) {
            } else {
                tabBezPotworzen[indeksBezPowtorzen++] = tab[i];
            }
        }

        System.out.println(Arrays.toString(tab));
        //System.out.println(Arrays.toString(tabBezPotworzen));

        int[] tabBezPowtIZer = new int[indeksBezPowtorzen];

        for (int i = 0; i < indeksBezPowtorzen; i++) {
            tabBezPowtIZer[i] = tabBezPotworzen[i];
        }
        System.out.println(Arrays.toString(tabBezPowtIZer));
    }

    public static void main(String[] args) {

        TablicaBezDuplikatow tabelka = new TablicaBezDuplikatow();
        tabelka.usunDuplikaty();
    }
}
