package tablice;

import java.util.Scanner;

public class RoznicaMinMax {

    // liczy różnicę pomiędzy największa i najmniejszą wartością z tablicy

    public int obliczRoznice() {

        Scanner sc = new Scanner(System.in);
        System.out.println("podaj rozmiar tablicy: ");
        int rozmiar = sc.nextInt();
        int[] tab = new int[rozmiar];
        System.out.println("Podaj kolejno wartosci: ");
        for (int i = 0; i < rozmiar; i++) {
            System.out.println("Wpisz wartosc nr: " + (i + 1));
            int x = sc.nextInt();
            tab[i] = x;
        }

        int max = tab[0];
        int min = tab[0];

        for (int i : tab) {
            if (i > max) {
                max = i;
            }
            if (i < min) {
                min = i;
            }
        }
        int wynik = max - min;
        return wynik;
    }

    public static void main(String[] args) {

        RoznicaMinMax tablica = new RoznicaMinMax();
        System.out.println("Różnica pomiędzy największą i najmniejszą wartością: " + tablica.obliczRoznice());
    }
}
